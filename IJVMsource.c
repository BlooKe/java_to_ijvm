/*
 * =====================================================================================
 *       Filename:  IJVMsource.c
 *    Description:  Java bytecode coverted to IJVM asm code
 *        Created:  2015-01-02 16:03
 *         Author:  Lauris Radzevics        (radzevics.lauris@gmail.com)
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
    char *key;
    char *value;
} map_table_t;

typedef struct{
    int line;
    char command[255];
    char value[50];
} line_table_t;

char* mapCommand (map_table_t *Mapping, char *command){
    while(NULL != Mapping->key)
    {
        if (0 == strncmp(Mapping->key, command, strlen(Mapping->key)))
        {
            return Mapping->value;
        }
        Mapping++;
    }
    return "UNKNOWN";
}

void deleteSpaces(char src[], char dst[]){
    int s, d=0;
    for (s=0; src[s] != 0; s++)
        if (src[s] != ' ' || src[s] != '\n') {
            dst[d] = src[s];
            d++;
        }
        dst[d] = 0;
}

void replace_char (char *s, char find, char replace) {
    while (*s != 0) {
        if (*s == find)
            *s = replace;
        s++;
    }
}

int main(int argc, const char **argv)
{
    FILE *java_fp, *jas_fb;
    size_t len = 0;
    ssize_t read=0;
    int del_line = 6;
    char *line = NULL;
    char *value = NULL;
    char variable_def[20][10];
    char variables[20][10];
    int i=0, e=0, j=0, k=0;
    int line_count = 0;
    int offsets[255];
    int offset_count = 0;
    int add_offset = -1;
    char tmp_line_nr[4];
    line_table_t jas_fb_lines[255];

    map_table_t Mapping[] = {
        {"iadd", "IADD"},
        {"isub", "ISUB"},
        {"imul", "IMUL"},
        {"iinc", "IINC"},
        {"iand", "IAND"},
        {"ior", "IOR"},
        {"invokevirtual", "INVOKEVIRTUAL"},
        {"ireturn", "IRETURN"},
        {"goto", "GOTO"},
        {"ifeq", "IFEQ"},
        {"iflt", "IFLT"},
        {"ifle", "IFLE"},
        {"ifge", "IFGE"},
        {"if_icmpeq", "IF_ICMPEQ"},
        {"if_icmpne", "IF_ICMPNE"},
        {"if_icmplt", "IF_ICMPLT"},
        {"iload", "ILOAD"},
        {"istore", "ISTORE"},
        {"ldc_w", "LDC_W"},
        {"iconst", "BIPUSH"},
        {"dup", "DUP"},
        {"swap", "SWAP"},
        {"pop", "POP"},
        {"nop", "NOP"},
        {"wide", "WIDE"},
        {"bipush", "BIPUSH"},
        {"ixor", "IXOR"},
        {"return", ""},
        {NULL}
    };

    memset(&variable_def, 0, sizeof(variable_def));
    memset(&variables, 0, sizeof(variables));
    memset(&jas_fb_lines, 0, sizeof(jas_fb_lines));

    java_fp = fopen(argv[1],"r"); 
    jas_fb = fopen(argv[2],"w+");

    if( java_fp == NULL || jas_fb == NULL )
    {
         perror("Error while opening the files.\n");
         exit(EXIT_FAILURE);
    }

    /* Read file line by line */   
    while( (read = getline(&line, &len, java_fp)) != -1 )
    {
        char nospace_line[read];

        /*  remove first 6 lines */
        if (del_line)
        {
            del_line--;
        }
        else
        {
            deleteSpaces(&line[10], nospace_line); 
            
            /* get value */
		    value = strpbrk (nospace_line+3, "_0123456789");
		    if(value)
		    {
		        if(value[0] == '_')
			    value++;

		        if(0 == strncmp(nospace_line, "iinc", 4))
		        {
			        replace_char(value, ',', ' ');
		        }

		        strncpy(jas_fb_lines[line_count].value, value, strlen(value)-1);
		    }

            /*  Save mapped command */    
		    sprintf(jas_fb_lines[line_count].command, "%s", mapCommand(Mapping, nospace_line));
		
		    /* Line numbers */
		    strncpy(tmp_line_nr, &line[5],3);
		    jas_fb_lines[line_count].line = atoi(tmp_line_nr);

		    /*  Catch offsets */
		    if(0 == strncmp(nospace_line, "goto", 4)
		        || 0 == strncmp(nospace_line, "ifeq", 4)
		        || 0 == strncmp(nospace_line, "iflt", 4)
		        || 0 == strncmp(nospace_line, "ifle", 4)
		        || 0 == strncmp(nospace_line, "ifge", 4)
		        || 0 == strncmp(nospace_line, "if_icmpeq", 8)
		        || 0 == strncmp(nospace_line, "if_icmpne", 8)
		        || 0 == strncmp(nospace_line, "if_icmplt", 8) )
		    {
		        offsets[offset_count] = atoi(value);
		        offset_count++;
		    }

		    /*  Collect variables */
		    if(0 == strncmp(nospace_line, "istore", 5))
		    {
		        deleteSpaces(value, variable_def[i]);
		        i++;    
		    }

		    line_count++;
        }
    }

    fprintf(jas_fb, ".main\n.var\n");

    /* Save only unique variables  */
    for (e = 0; e < i; e++) {
        for (j = e + 1; j < i;) {
            if (0 == strcmp(variable_def[j], variable_def[e])) {
                for (k = j; k < i; k++) {
                    strcpy(variable_def[k], variable_def[k + 1]);
                }
                i--;
            } else
                j++;
        }
    }

    /* print variables in file */
    while(i >= 0)
    {
        fprintf(jas_fb, "%s", variable_def[i]);
        i--;
    }

    fprintf(jas_fb, ".end-var\n");

    /* Print main functionality in jas file */
    i = 0;
    e = 0;
    while (i<line_count-1) {
        /* check if offet is used for thi line */
        while(e<offset_count)
        {
            if(offsets[e] == jas_fb_lines[i].line)
                add_offset = jas_fb_lines[i].line;
            e++;
        }

        /* Print IJVM commands */
        if(add_offset>=0)
        {
            fprintf(jas_fb, "%d:\t%s\t%s\n", jas_fb_lines[i].line, jas_fb_lines[i].command, jas_fb_lines[i].value);
        }
        else
        {
            fprintf(jas_fb, "\t%s\t%s\n", jas_fb_lines[i].command, jas_fb_lines[i].value);
        }
        i++;
        e=0;
        add_offset = -1;
    }

    fprintf(jas_fb, ".end-main");

    /* Clean up */
    fclose(java_fp);
    fclose(jas_fb);
    free(line);

    return 0;
}


